var rocket;

function setup() {
	createCanvas(500, 500);
	population = new Population;
}

function draw() {
	background(0);
	this.rocket.update();
	this.rocket.show();
}

function Population(){
	this.rockets = [];
	this.size = 100;

	for (var i = 0; i < this.size; i++){
		rockets[i] = new Rocket();
	}

}

function Rocket(){
	this.pos = createVector(width/2, height);
	this.vel = createVector(0, -1, 0);
	this.acc = createVector();

	this.applyForce = function(force){
		this.acc.add(force);
	}

	this.update = function(){
		this.vel.add(this.acc);
		this.pos.add(this.vel);
		this.acc.mult(0);
	}

	this.show = function(){
		push();
		translate(this.pos.x, this.pos.y)
		rotate(this.vel.heading());
		rectMode(CENTER);
		rect(0, 0, 50, 10);
		pop();
	}
}