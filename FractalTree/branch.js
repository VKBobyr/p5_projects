function Branch(beginning, length, angle, ind, cangle, ratio){
    this.b = beginning;
    this.l = length;
    this.a = angle;
    this.i = ind;
    // this.c = cangle;

    this.indexLimit = 11;

    // this.show();

    this.createBranches = function(newB){
        if (this.i < this.indexLimit){
            this.i++;
            this.branchA = new Branch(newB, this.l * ratio, this.a + cangle, this.i, cangle, ratio);
            this.branchB = new Branch(newB, -this.l * ratio, this.a + cangle + 90, this.i, cangle, ratio);
            ellipse(0, -5, 5, -5);        
        }
    }

    this.show = function(){
        this.v;
        if (this.i == 0){
            this.v = createVector(this.b.x, this.b.y - this.l);
        }
        else{
            this.v = createVector(this.b.x + this.l * cos(this.a), this.b.y - this.l * sin(this.a));
        }

        
        

        stroke (cos(this.i) * 255, sin(this.i) * 255, 255);
        // stroke(cos(this.b.x) * 255, sin(this.b.y) * 255, 0);
        // stroke(255);
        line(this.b.x, this.b.y, this.v.x, this.v.y);


        this.createBranches(this.v);

    }
    this.show();
}