var a;
var s;
function setup() {
	createCanvas(500, 500);
	angleMode(DEGREES);
	s1 = createSlider(-45, 45, 0, 1);
	s2 = createSlider(0, 200, 100, 5);
	s3 = createSlider(.4, .8, .75, .01);
	strokeWeight(2);
	colorMode(HSB);
	
}
function draw() {
	background(0);
	
	a = s1.value();
	l = s2.value();
	r = s3.value();
	translate(width/2, height);

	var branch = new Branch(createVector(0, 0), l, 90, 0, 45 - a, r);
	ellipse(0, 0, 5, 5);

	
}

