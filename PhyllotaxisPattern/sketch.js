var n = 1;
var c = 3;
w = 1;
h = 1;

function setup() {
	createCanvas(600, 600);
	background(0);
	angleMode(degrees);
	colorMode(HSB);
}

function draw() {
	theta = n * 137.5;
	r = c * sqrt(n);

	x = r * cos(theta) + width / 2;
	y = r * sin(theta) + height / 2;

	w = r * c / r;
	h = r * c / r;

	// Circles

	fill(theta%256, 255, 255);
	noStroke();
	ellipse(x, y, w, h);
	n++;
	
}

function sigmoid(x) {
	return 1 / (1 + Math.pow(Math.E, -x));
}

