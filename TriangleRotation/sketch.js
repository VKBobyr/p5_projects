

var drawer;

function setup() {
	createCanvas(500, 500);
	background(255);
	drawer = new Drawer();
}

function draw() {
	fill(255);
	draw
	drawer.drawRay();

	fill(5);
	//ellipse(width/2, height/2, 10, 10);
}
function Drawer(){
	this.length = 250;
	this.direction = createVector(0, 1);
	this.position = createVector(width/2,height/2);

	this.drawRay = function(){
		this.dVect = this.direction.mult(5);
		line(this.position.x, this.position.y, this.dVect.x, this.dVect.y);
	}
}
