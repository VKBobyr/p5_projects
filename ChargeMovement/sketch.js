var physics;
var charge;

function setup() {
	createCanvas(500, 500);
	//charge = new Charge(1, 2);
	physics = new Physics();
}

function draw() {
	background(0);
	
	//charge.update();
	physics.update();
}

function MagneticField(){
	this.t = createVector(1, 1, 1);
}

function Physics(){
	this.charges = [];
	this.magneticField = new MagneticField();
	
	this.charges[0] = new Charge(1, 1);
	
	this.update = function(){
		for (var i = 0; i < 1; i++){
			this.charges[i].applyForce(this.magneticForce(i));
			this.charges[i].update();
		}
	}

	this.magneticForce = function(ind){
		this.v = this.charges[ind].vel;
		this.c = this.charges[ind].q;
		this.b = this.magneticField.t;

		// return createVector(0);
		return (this.v.cross(this.b)).mult(this.c);
		// return createVector(.1, .1, .1);
	}

}

function Charge(c, kg){
	this.q = c;
	this.m = kg;
	
	this.pos = createVector(width / 2, height / 2);
	this.vel = createVector(1, 1, 0);
	this.acc = createVector();

	this.update = function(){
		this.vel.add(this.acc);
		this.pos.add(this.vel);

		this.show();
	}

	this.show = function(){
		push();
		translate(this.pos.x, this.pos.y);
		ellipse(0, 0, 10, 10);
		noStroke();
		pop();
	}

	this.applyForce = function(force){
		// this.acc.add(force);
		// this.acc.add(force.mult(1 / this.m));
		this.acc = force / this.m;
	}
}

