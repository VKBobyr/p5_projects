function Charge(){
	this.q = 1;
	this.m = 1;
	
	this.pos = createVector(width / 2, height / 2);
	this.vel = createVector(1, 1, 0);
	this.acc = createVector();

	this.update = function(){
		this.vel.add(this.acc);
		this.pos.add(this.vel);
		this.acc.mult(0);
	}

	this.show = function(){
		push();
		translate(this.pos.x, this.pos.y);
		ellipse(0, 0, 10, 10);
		noStroke();
		pop();
	}

	this.applyForce = function(force){
		acc.add(force.mult(1 / this.m));
	}
}